# Changelog

## Unreleased
* Add FastAPI methods for direct testing

## 0.0.1: 2023-09-25
* Basic interface design